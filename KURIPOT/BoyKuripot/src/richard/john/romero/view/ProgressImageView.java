package richard.john.romero.view;

import boy.kuripot.R;
import richard.john.romero.model.ImageLoaderListener;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class ProgressImageView extends RelativeLayout implements ImageLoaderListener
{
	
	private ProgressBar pb;
	private ImageView iv;
	private Handler handler;

	public ProgressImageView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		// TODO Auto-generated constructor stub
		inflate(context, R.layout.progress_imageview, this);
		pb = (ProgressBar) findViewById(R.id.progress_imageview_progress);
		iv = (ImageView) findViewById(R.id.progress_imageview_image);
		handler = new Handler();
	}
	
	public void setImageDrawable(final Drawable drawable)
	{
		handler.post(new Runnable()
		{
			
			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				if (drawable == null)
					iv.setImageDrawable(getContext().getResources().getDrawable(R.drawable.aboutus));
				else
					iv.setImageDrawable(drawable);
				iv.setVisibility(ImageView.VISIBLE);
				pb.setVisibility(ProgressBar.GONE);
			}
		});
	}

	@Override
	public void setImage(final Drawable drawable)
	{
		// TODO Auto-generated method stub
		handler.post(new Runnable()
		{
			
			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				iv.setImageDrawable(drawable);
				iv.setVisibility(ImageView.VISIBLE);
				pb.setVisibility(ProgressBar.GONE);
			}
		});
	}

	@Override
	public void unsetImage()
	{
		// TODO Auto-generated method stub
		iv.setVisibility(ImageView.INVISIBLE);
		pb.setVisibility(ProgressBar.VISIBLE);
	}

}
