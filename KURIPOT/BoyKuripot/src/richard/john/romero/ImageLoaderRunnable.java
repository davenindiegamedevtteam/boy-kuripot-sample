package richard.john.romero;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;

import richard.john.romero.model.ImageReference;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;

public class ImageLoaderRunnable implements Runnable
{
//	private final Pattern pattern = Pattern.compile("(?<=src=\")[^\"]*(?=\")");
	private Context _context;
	private String _dirPath;
	
	public ImageLoaderRunnable(Context context, String dirPath)
	{
		// TODO Auto-generated constructor stub
		_context = context;
		_dirPath = dirPath;
	}

	@Override
	public synchronized void run()
	{
		// TODO Auto-generated method stub
		while (true)
		{
			try
			{
				if (ImageLoader.refQueue.size() == 0)
				{
					break;
				}
				
				ImageReference imgRef;
				synchronized (ImageLoader.refQueue)
				{
					imgRef = ImageLoader.refQueue.poll();
				}
				if (imgRef != null)
				{
					String url = imgRef.getImageUrl();
					BitmapDrawable bd = new BitmapDrawable(_context.getResources(), getBitmap(url));
					if (bd != null)
					{
						synchronized (ImageLoader.cacheRef)
						{
							ImageLoader.cacheRef.put(url, bd);
						}
						imgRef.getListener().setImage(bd);
					}
				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public Bitmap getBitmap(String imageUrl)
	{
		if (imageUrl == null)
			return null;
		
		Bitmap bmp = null;
		String filename = String.valueOf(imageUrl.hashCode());
		File f = new File(_dirPath, filename);
		
		bmp = BitmapFactory.decodeFile(f.getPath());
		if (bmp != null)
			return bmp;
		
		try
		{
			bmp = BitmapFactory.decodeStream(
					new URL(imageUrl).openConnection().getInputStream());
			if (bmp != null)
			{
				writeFile(bmp, f);
			}
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bmp;
	}
	
	private void writeFile(Bitmap bmp, File f) {
		FileOutputStream out = null;
		
		try {
			out = new FileOutputStream(f);
			bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally { 
			try { if (out != null ) out.close(); }
			catch(Exception ex) {} 
		}
	}
	
}