package richard.john.romero;

import java.io.File;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import richard.john.romero.model.ImageLoaderListener;
import richard.john.romero.model.ImageReference;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;

public class ImageLoader
{
	private static BlockingQueue<Runnable> queue =
			new ArrayBlockingQueue<Runnable>(4);
	
	private static int MAX_POOL_SIZE = 3;
	private static int BUFFER = 1;
    private static ThreadPoolExecutor exeService =
    		new ThreadPoolExecutor(1, MAX_POOL_SIZE + BUFFER, 20, TimeUnit.SECONDS, queue);
    
    static Queue<ImageReference> refQueue =
    		new ConcurrentLinkedQueue<ImageReference>();
    
    private static int cacheSize = 4 * 1024 * 1024;
    static LruCache<String, Drawable> cacheRef =
    		new LruCache<String, Drawable>(cacheSize);
    private Context context;
	private String dirPath;
	
	
	public ImageLoader(Context context, String externalStorageCachePath)
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		File cacheDir;
		String sdState = android.os.Environment.getExternalStorageState();
		if (sdState.equals(android.os.Environment.MEDIA_MOUNTED))
		{
			File sdDir = android.os.Environment.getExternalStorageDirectory();		
			cacheDir = new File(sdDir, externalStorageCachePath);
		}
		else
			cacheDir = context.getCacheDir();
		
		if(!cacheDir.exists())
			cacheDir.mkdirs();
		
		dirPath = cacheDir.getPath();
	}
	
	public synchronized void displayImage(String imageUrl, ImageLoaderListener listener)
	{
		if (imageUrl == null)
		{
			listener.setImage(null);
			return;
		}
		ImageReference imgRef = new ImageReference(imageUrl, listener);
		Drawable drawable = cacheRef.get(imageUrl);
		if (drawable != null)
		{
			listener.setImage(drawable);
//			Log.i("ImageLoader", "cacheRef has this drawable");
		}
		else 
		{
			refQueue.add(imgRef);
			if (exeService.getActiveCount() <= MAX_POOL_SIZE)
			{
				try
				{
					
					exeService.submit(new ImageLoaderRunnable(context, dirPath));
				}
				catch (RejectedExecutionException e)
				{
					e.printStackTrace();
				}
			}
			listener.unsetImage();
		}
//		int exeSize = exeService.getActiveCount();
//		int refSize = refQueue.size();
//		int cacheSize = cacheRef.size();
//		Log.i("ImageLoader", String.valueOf("# of runnables: " + exeSize));
//		Log.i("ImageLoader", String.valueOf("queued tasks: " + refSize));
//		Log.i("ImageLoader", String.valueOf("cache size: " + cacheSize));
	}

}
