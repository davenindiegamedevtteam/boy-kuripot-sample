package richard.john.romero.model;

import android.graphics.drawable.Drawable;

public interface ImageLoaderListener
{
	public void setImage(Drawable drawable);
	public void unsetImage();
}
