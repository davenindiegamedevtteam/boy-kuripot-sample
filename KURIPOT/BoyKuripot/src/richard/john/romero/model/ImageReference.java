package richard.john.romero.model;


public class ImageReference
{
	private String imageUrl;
	private ImageLoaderListener listener;
	
	public ImageReference(String imageUrl, ImageLoaderListener listener)
	{
		this.imageUrl = imageUrl;
		this.listener = listener;
	}
	public String getImageUrl()
	{
		return imageUrl;
	}
	
	public ImageLoaderListener getListener()
	{
		return listener;
	}
	
}
