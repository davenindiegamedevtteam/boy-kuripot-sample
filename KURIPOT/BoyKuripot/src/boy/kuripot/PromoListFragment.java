package boy.kuripot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import com.actionbarsherlock.app.ActionBar;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import boy.kuripot.controller.CcPromoListArrayAdapter;
import boy.kuripot.controller.PromoListItemClickListener2;
import boy.kuripot.controller.PromoListLoader;
import boy.kuripot.controller.PromoListRefreshListener;
import boy.kuripot.db.DBHelper;
import boy.kuripot.db.PromoDBHelper;
import boy.kuripot.model.Feed;
import boy.kuripot.model.FeedComparator;
import boy.kuripot.model.FragmentInfo;
import boy.kuripot.model.ParserEngine.Callback;
import boy.kuripot.model.PromoFragment;
import boy.kuripot.parser.engine.AtomParserEngine;
import boy.kuripot.view.AboutUsDialog;
import boy.kuripot.view.HeadlineView;

public class PromoListFragment extends PromoFragment
		implements Callback, OnClickListener,
					LoaderCallbacks<List<Feed>>
{
	private AtomParserEngine engine;
	private PullToRefreshListView lv;
	private static CcPromoListArrayAdapter adapter;
	private static ArrayList<Feed> _feeds = new ArrayList<Feed>();
	private Handler threadHandler;
	private PromoDBHelper helper;
	private HeadlineView curHeadlineView;
	private boolean isUpdated = false;

	@Override
	protected void initActionBar(ActionBar actionbar)
	{
		// TODO Auto-generated method stub
		actionbar.setCustomView(R.layout.about_us);
        actionbar.setDisplayOptions(
        		ActionBar.DISPLAY_SHOW_CUSTOM,
        		ActionBar.DISPLAY_SHOW_CUSTOM
        		| ActionBar.DISPLAY_SHOW_HOME
        		| ActionBar.DISPLAY_SHOW_TITLE);
        
        View aboutUsIv = actionbar.getCustomView().findViewById(R.id.about_us_iv);
        aboutUsIv.setOnClickListener(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        
        engine = new AtomParserEngine(this);
        helper = new PromoDBHelper(getActivity());
        
        if (savedInstanceState != null)
        {
        	_feeds = savedInstanceState.getParcelableArrayList(getString(R.string.feed_tag));
        }
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// TODO Auto-generated method stub
		outState.putParcelableArrayList(getString(R.string.feed_tag), _feeds);
		super.onSaveInstanceState(outState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		FragmentActivity a = getActivity();
		View v = inflater.inflate(R.layout.promo_list, container, false);
		lv = (PullToRefreshListView) v.findViewById(R.id.promo_list_lv);
        
        adapter = new CcPromoListArrayAdapter(a, _feeds);
        
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(
        		new PromoListItemClickListener2(
        				this, R.id.fragment_container,
        				new FragmentInfo(PromoInfoFragment.class,
        						"info")));
        lv.setOnRefreshListener(new PromoListRefreshListener(this));
        
        if (_feeds.isEmpty())
        	a.getSupportLoaderManager().initLoader(0, null, this);
        
        
		return v;
	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub
		sendMessage(R.id.quit, null);
		threadHandler = null;
		super.onStop();
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		try
		{
			new Thread((Runnable) engine.clone()).start();
		} catch (CloneNotSupportedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(Observable observable, Object data)
	{
		// TODO Auto-generated method stub
		
		if (data == null)
			return;
		if (!isDetached())
		{
			if (data instanceof View)
			{
				Log.i(getTag(), "is instanceof view");
				View v = (View) data;
				View tempV = v.findViewById(R.id.promo_list_item_headline_view);
				if (tempV != null)
					curHeadlineView = (HeadlineView) tempV;
			}
			if (data instanceof Integer && curHeadlineView != null)
			{
				Log.i(getTag(), "is instanceof integer");
				TextView tv = (TextView) curHeadlineView.findViewById(R.id.headline_view_manage_tv);
				tv.setText(String.valueOf(data));
			}
		}
		else
			Log.i(getTag(), "is detached");
	}

	@Override
	public void onHandlerInit(Handler handler)
	{
		// TODO Auto-generated method stub
		threadHandler = handler;
		sendMessage(R.id.request_feed, getString(R.string.url));
	}

	@Override
	public void onParseFinished(List<Feed> feeds)
	{
		// TODO Auto-generated method stub
//		cleanList(feeds);
		helper.addFeeds(feeds);
		getActivity().getSupportLoaderManager().restartLoader(0, null, this);
//		if (!_feeds.isEmpty())
//			_feeds.clear();
//		_feeds.addAll(feeds);
//		
		lv.post(new Runnable()
		{
			
			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				if (adapter != null)
				{
					lv.onRefreshComplete();
//					adapter.notifyDataSetChanged();
				}
			}
		});
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.about_us_iv:
			FragmentActivity a = getActivity();
			new AboutUsDialog().show(a.getSupportFragmentManager(), null);
			break;
		}
	}

	@Override
	public Loader<List<Feed>> onCreateLoader(int id, Bundle bundle)
	{
		// TODO Auto-generated method stub
		return new PromoListLoader(getActivity());
	}

	@Override
	public void onLoadFinished(Loader<List<Feed>> loader, List<Feed> feeds)
	{
		// TODO Auto-generated method stub
		if (adapter != null && !feeds.isEmpty())
		{	
			if (!_feeds.isEmpty())
				_feeds.clear();
			_feeds.addAll(feeds);
			adapter.notifyDataSetChanged();
		}
//		sendMessage(R.id.request_feed, getString(R.string.url));
	}

	@Override
	public void onLoaderReset(Loader<List<Feed>> loader)
	{
		// TODO Auto-generated method stub
		if (!_feeds.isEmpty())
			_feeds.clear();
	}
	
	public void sendMessage(int msgId, Object object)
	{
		if (threadHandler != null)
		{
			Message msg = Message.obtain(threadHandler, msgId, object);
			msg.sendToTarget();
		}
	}
	
	private void cleanList(List<Feed> feeds)
	{
		for (Feed feed : feeds)
		{
			boolean duplicate = false;
//			for (Feed _feed : _feeds)
			for (int i=0; i< _feeds.size(); i++)
			{
				Feed _feed = _feeds.get(i);
				if (_feed.getBlogEntryId().equals(feed.getBlogEntryId()))
				{
					if (_feed.getDateUpdated() == null || !_feed.getDateUpdated().equals(feed.getDateUpdated()))
					{
						helper.updateFeed(feed);
						_feeds.remove(i);
						_feeds.add(i, feed);
					}
					duplicate = true;
					break;
				}
			}
			if (!duplicate)
			{
				_feeds.add(feed);
//				if (helper == null)
//					helper = new PromoDBHelper(getActivity());
				helper.addFeed(feed);
			}
		}
		Collections.sort(_feeds, new FeedComparator());
	}

}
