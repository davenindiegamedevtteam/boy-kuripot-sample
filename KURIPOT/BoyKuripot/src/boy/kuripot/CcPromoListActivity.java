package boy.kuripot;

import java.util.ArrayList;
import java.util.List;

import boy.kuripot.R;
import boy.kuripot.controller.CcPromoListArrayAdapter;
import boy.kuripot.controller.PromoListItemClickListener;
import boy.kuripot.controller.PromoListLoader;
import boy.kuripot.db.PromoDBHelper;
import boy.kuripot.model.Feed;
import boy.kuripot.model.ParserEngine.Callback;
import boy.kuripot.parser.engine.AtomParserEngine;
import boy.kuripot.view.AboutUsDialog;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;

public class CcPromoListActivity extends SherlockFragmentActivity
		implements OnRefreshListener<ListView>, Callback,
					LoaderCallbacks<List<Feed>>, OnClickListener
{
	private String url = "http://feeds2.feedburner.com/phcreditcardpromos";
	private AtomParserEngine engine;
	private PullToRefreshListView lv;
	private static CcPromoListArrayAdapter adapter;
	private static List<Feed> _feeds;
	private Handler threadHandler;
	private PromoDBHelper helper = new PromoDBHelper(this);

	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promo_list);
        
        ActionBar actionbar = getSupportActionBar();
        actionbar.setCustomView(R.layout.about_us);
        actionbar.setDisplayOptions(
        		ActionBar.DISPLAY_SHOW_CUSTOM,
        		ActionBar.DISPLAY_SHOW_CUSTOM
        		| ActionBar.DISPLAY_SHOW_HOME
        		| ActionBar.DISPLAY_SHOW_TITLE);
        
        engine = new AtomParserEngine(this);
        
        View aboutUsIv = findViewById(R.id.about_us_iv);
        aboutUsIv.setOnClickListener(this);
        
        lv = (PullToRefreshListView) findViewById(R.id.promo_list_lv);
        
        _feeds = new ArrayList<Feed>();
        adapter = new CcPromoListArrayAdapter(this, _feeds);
        
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new PromoListItemClickListener(this));
        lv.setOnRefreshListener(this);
        
        getSupportLoaderManager().initLoader(0, null, this);
    }

	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
		sendMessage(R.id.quit, null);
		super.onStop();
	}

	@Override
	protected void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		try
		{
			new Thread((Runnable) engine.clone()).start();
		} catch (CloneNotSupportedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause()
	{
		// TODO Auto-generated method stub
		
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView)
	{
		// TODO Auto-generated method stub
		if (threadHandler != null)
		{
			sendMessage(R.id.request_feed, url);
		}
	}

	@Override
	public void onHandlerInit(Handler handler)
	{
		// TODO Auto-generated method stub
		threadHandler = handler;
//		if (isFirstRun)
//		{
//			sendMessage(R.id.request_feed, url);
//			isFirstRun = false;
//		}
	}

	@Override
	public void onParseFinished(final List<Feed> feeds)
	{
		// TODO Auto-generated method stub
		cleanList(feeds);
		runOnUiThread(new Runnable()
		{
			
			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				if (adapter != null)
				{
					lv.onRefreshComplete();
//					if (!adapter.isEmpty())
//						adapter.clear();
//					adapter.addAll(feeds);
//					if (!_feeds.isEmpty())
//						_feeds.clear();
//					_feeds.addAll(feeds);
					
					adapter.notifyDataSetChanged();
				}
			}
		});
//		helper.addFeeds(feeds);
	}
	
	private void cleanList(List<Feed> feeds)
	{
		for (Feed feed : feeds)
		{
			boolean duplicate = false;
			for (Feed _feed : _feeds)
			{
				if (_feed.getBlogEntryId().equals(feed.getBlogEntryId()))
				{
					duplicate = true;
					break;
				}
			}
			if (!duplicate)
			{
				_feeds.add(feed);
				helper.addFeed(feed);
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		// TODO Auto-generated method stub
//		super.onActivityResult(requestCode, resultCode, intent);
		switch (resultCode)
		{
		case Activity.RESULT_OK:
			Log.i("PromoListActivity", "result ok");
			break;
		case Activity.RESULT_CANCELED:
			Log.i("PromoListActivity", "result cancelled");
			break;
		case Activity.RESULT_FIRST_USER:
			Log.i("PromoListActivity", "result first user");
			break;
		}
		
		if (intent != null)
		{
			Feed feed = intent.getParcelableExtra(getString(R.string.feed_tag));
			for (int i=0; i<_feeds.size(); i++)
			{
				Feed _feed = _feeds.get(i);
				if (_feed.getBlogEntryId().equals(feed.getBlogEntryId()))
				{
					Log.i("PromoListActivity", "feed found");
					_feeds.set(i, feed);
					adapter.notifyDataSetChanged();
					break;
				}
			}
		}
	}

	private void sendMessage(int msgId, Object object)
	{
		if (threadHandler != null)
		{
			Message msg = Message.obtain(threadHandler, msgId, object);
			msg.sendToTarget();
		}
	}

	@Override
	public Loader<List<Feed>> onCreateLoader(int id, Bundle bundle)
	{
		// TODO Auto-generated method stub
		return new PromoListLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<List<Feed>> loader, List<Feed> feeds)
	{
		// TODO Auto-generated method stub
		if (adapter != null)
		{	
			_feeds.addAll(feeds);
			adapter.notifyDataSetChanged();
		}
		sendMessage(R.id.request_feed, url);
	}

	@Override
	public void onLoaderReset(Loader<List<Feed>> loader)
	{
		// TODO Auto-generated method stub
		if (!_feeds.isEmpty())
			_feeds.clear();
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.about_us_iv:
			new AboutUsDialog().show(getSupportFragmentManager(), null);
			break;
		case R.id.headline_view_manage_tv:
//			new ChargeSlipDialog(this).show(getSupportFragmentManager(), null);
			break;
		}
	}

}
