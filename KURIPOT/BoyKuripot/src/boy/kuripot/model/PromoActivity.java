package boy.kuripot.model;

import java.util.Observable;


import com.actionbarsherlock.app.SherlockFragmentActivity;

public class PromoActivity extends SherlockFragmentActivity
		implements OnContentChangedCallback
{
	private ScreenMode screenMode = ScreenMode.DUAL_PANE;
	private Observable observable = new PromoObservable();

	public Observable getObservable()
	{
		return observable;
	}

	@Override
	public void onContentChanged(Object object)
	{
		// TODO Auto-generated method stub
		observable.notifyObservers(object);
	}

	public ScreenMode getScreenMode()
	{
		return screenMode;
	}

	public void setScreenMode(ScreenMode screenMode)
	{
		this.screenMode = screenMode;
	}

	public void setObservable(Observable observable)
	{
		this.observable = observable;
	}
	
}
