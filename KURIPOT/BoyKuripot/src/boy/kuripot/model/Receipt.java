package boy.kuripot.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Receipt implements Parcelable
{
	private String blogEntryId;
	private long dateCreated = 0, dateClaimed = 0;

	public Receipt()
	{
		// TODO Auto-generated constructor stub
	}
	
	public Receipt(Parcel source)
	{
		// TODO Auto-generated constructor stub
		this.blogEntryId = source.readString();
		this.dateCreated = source.readLong();
		this.dateClaimed = source.readLong();
	}

	public String getBlogEntryId()
	{
		return blogEntryId;
	}

	public void setBlogEntryId(String blogEntryId)
	{
		this.blogEntryId = blogEntryId;
	}

	public long getDateCreated()
	{
		return dateCreated;
	}

	public void setDateCreated(long dateCreated)
	{
		this.dateCreated = dateCreated;
	}

	public long getDateClaimed()
	{
		return dateClaimed;
	}

	public void setDateClaimed(long dateClaimed)
	{
		this.dateClaimed = dateClaimed;
	}
	
	public String toString()
	{
		return new StringBuilder("Blog Entry Id:")
				.append(blogEntryId).append(", ")
				.append("Date Created:")
				.append(dateCreated).append(", ")
				.append("Date Claimed:")
				.append(dateClaimed).toString();
	}

	@Override
	public int describeContents()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		// TODO Auto-generated method stub
		dest.writeString(blogEntryId);
		dest.writeLong(dateCreated);
		dest.writeLong(dateClaimed);
	}
	
	public static final Creator<Receipt> CREATOR = new Creator<Receipt>() {

		@Override
		public Receipt createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Receipt(source);
		}

		@Override
		public Receipt[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Receipt[size];
		}
	};
	
}
