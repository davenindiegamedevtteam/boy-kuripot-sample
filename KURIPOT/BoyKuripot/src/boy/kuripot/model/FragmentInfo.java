package boy.kuripot.model;

public class FragmentInfo
{
	private Class<?> clz;
	private String tag;
	
	public FragmentInfo(Class<?> clz, String tag)
	{
		// TODO Auto-generated constructor stub
		this.clz = clz;
		this.tag = tag;
	}
	
	public Class<?> getClz()
	{
		return clz;
	}
	
	public void setClz(Class<?> clz)
	{
		this.clz = clz;
	}
	
	public String getTag()
	{
		return tag;
	}
	
	public void setTag(String tag)
	{
		this.tag = tag;
	}
}
