package boy.kuripot.model;

import java.util.Comparator;

public class FeedComparator implements Comparator<Feed>
{

	@Override
	public int compare(Feed lhs, Feed rhs)
	{
		// TODO Auto-generated method stub
		return lhs.getDateDateUpdated().compareTo(rhs.getDateDateUpdated());
	}

}
