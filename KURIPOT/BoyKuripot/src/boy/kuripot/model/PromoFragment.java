package boy.kuripot.model;

import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public abstract class PromoFragment extends SherlockFragment
		implements Observer
{
	private PromoActivity activity;
	private OnContentChangedCallback callback;
	private Observable observable;
	private ScreenMode screenMode;

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try
		{
			this.activity = (PromoActivity) activity;
			callback = this.activity;
			observable = this.activity.getObservable();
			observable.addObserver(this);
			
//			ActionBar actionbar = a.getSupportActionBar();
//			initActionBar(actionbar);
//			if (!abIsInit)
//			{
//				View v = actionbar.getCustomView();
//				if (v != null)
//					initActionBarCustomView(v);
//				abIsInit = true;
//			}
			Log.i(getTag(), "onAttach");
		}
		catch (ClassCastException e)
		{
			throw new ClassCastException(
					"Activity must be of type PromoActivity");
		}
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		screenMode = activity.getScreenMode();
		if (screenMode == ScreenMode.DUAL_PANE)
			return;
		SherlockFragmentActivity a = getSherlockActivity();
		ActionBar actionbar = a.getSupportActionBar();
		initActionBar(actionbar);
		View v = actionbar.getCustomView();
		if (v != null)
			initActionBarCustomView(v);
	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub
		getSherlockActivity().invalidateOptionsMenu();
		super.onStop();
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub
		observable.deleteObserver(this);
		Log.i(getTag(), "onDestroy");
		super.onDestroy();
	}
	
	/**
	 * Initialize actionbar here.
	 * @param actionbar
	 */
	protected abstract void initActionBar(ActionBar actionbar);
	
	protected void initActionBarCustomView(View v)
	{
	}

	/**
	 * Should be called when feed content is changed.
	 * @param feed
	 */
	protected final void setContentChanged(Feed feed)
	{
		callback.onContentChanged(feed);
	}
	
	protected ScreenMode getScreenMode()
	{
		return screenMode;
	}

}
