package boy.kuripot.model;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BoyKuripotDialog extends DialogFragment
{
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME, 0);
	}

//	@SuppressLint("NewApi")
//	@Override
//	public Dialog onCreateDialog(Bundle savedInstanceState)
//	{
//		// TODO Auto-generated method stub
//		Context context = getActivity();
//		LayoutInflater inflater = LayoutInflater.from(context);
//		Builder builder = null;
//		int sdk_version = android.os.Build.VERSION.SDK_INT;
//		if (sdk_version >= android.os.Build.VERSION_CODES.HONEYCOMB)
//			builder = new Builder(context, android.R.style.Theme_DeviceDefault_Dialog);
//		else
//			builder = new Builder(context);
//		
//		View v = onInflateContent(inflater);
//		v.setBackgroundColor(Color.WHITE);
//		return builder.setView(v).create();		
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		View v = onInflateContent(inflater);
		return v;
	}

	/**
	 * Listeners for views should be setup on this method
	 * @param inflater
	 * @return an inflated layout with set listeners
	 */
	public abstract View onInflateContent(LayoutInflater inflater);

}
