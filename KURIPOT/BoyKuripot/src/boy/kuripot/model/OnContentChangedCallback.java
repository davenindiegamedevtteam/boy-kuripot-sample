package boy.kuripot.model;

public interface OnContentChangedCallback
{
	public void onContentChanged(Object object);
}
