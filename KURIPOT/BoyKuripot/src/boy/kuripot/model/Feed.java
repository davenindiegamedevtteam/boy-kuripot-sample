package boy.kuripot.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Feed implements Parcelable
{
	private final static DateFormat origDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private final static DateFormat newDateFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm aa");
	private String blogEntryId, title, datePublished,
			dateUpdated, imageUrl, content, url;
	private List<Receipt> receipts;

	public Feed()
	{
		// TODO Auto-generated constructor stub
		receipts = new ArrayList<Receipt>();
	}
	
	private Feed(Parcel source)
	{
		// TODO Auto-generated constructor stub
		this.blogEntryId = source.readString();
		this.title = source.readString();
		this.datePublished = source.readString();
		this.dateUpdated = source.readString();
		this.imageUrl = source.readString();
		this.content = source.readString();
		this.url = source.readString();
		if (receipts == null)
			receipts = new ArrayList<Receipt>();
		source.readTypedList(receipts, Receipt.CREATOR);
	}

	public String getBlogEntryId()
	{
		return blogEntryId;
	}

	public void setBlogEntryId(String blogEntryId)
	{
		this.blogEntryId = blogEntryId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDatePublished()
	{
		return datePublished;
	}
	
	public String getFormattedDatePublished()
	{
		String date = null;
		try
		{
			Date d = origDateFormat.parse(datePublished);
			date = newDateFormat.format(d);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public void setDatePublished(String date)
	{
		this.datePublished = date;
	}
	
	public String getDateUpdated()
	{
		return dateUpdated;
	}
	
	public Date getDateDateUpdated()
	{
		Date date = null;
		try
		{
			date = origDateFormat.parse(dateUpdated);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	public String getFormattedDateUpdated()
	{
		String date = null;
		try
		{
			Date d = origDateFormat.parse(dateUpdated);
			date = newDateFormat.format(d);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	public void setDateUpdated(String date)
	{
		this.dateUpdated = date;
	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public List<Receipt> getReceipts()
	{
		return receipts;
	}

	public void setReceipts(List<Receipt> receipts)
	{
		this.receipts = receipts;
	}

	@Override
	public int describeContents()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		// TODO Auto-generated method stub
		dest.writeString(blogEntryId);
		dest.writeString(title);
		dest.writeString(datePublished);
		dest.writeString(dateUpdated);
		dest.writeString(imageUrl);
		dest.writeString(content);
		dest.writeString(url);
		dest.writeTypedList(receipts);
	}
	
	public static final Creator<Feed> CREATOR = new Creator<Feed>() {

		@Override
		public Feed createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Feed(source);
		}

		@Override
		public Feed[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Feed[size];
		}
	};

}
