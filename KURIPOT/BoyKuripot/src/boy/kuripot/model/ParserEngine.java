package boy.kuripot.model;

import java.util.List;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import boy.kuripot.R;

public abstract class ParserEngine
		implements Runnable, Handler.Callback,
		Cloneable
{
	protected Callback callback;
	private Handler handler;
	private boolean isInitialized = false;

	public ParserEngine(Callback callback)
	{
		// TODO Auto-generated constructor stub
		this.callback = callback;
	}

	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		Looper.prepare();
		handler = new Handler(this);
		doOnce();
		Looper.loop();
	}

	@Override
	public boolean handleMessage(Message msg)
	{
		// TODO Auto-generated method stub
		switch (msg.what)
		{
		case R.id.quit:
			Looper.myLooper().quit();
			handler.removeMessages(R.id.quit);
			break;
		default:
			onMessageReceived(msg);	
		}
		return false;
	}
	
	protected abstract void onMessageReceived(Message msg);
	
	private void doOnce()
	{
		if (!isInitialized)
		{
			callback.onHandlerInit(handler);
			isInitialized = true;
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		// TODO Auto-generated method stub
		return super.clone();
	}

	public interface Callback
	{
		public void onHandlerInit(Handler handler);
		public void onParseFinished(List<Feed> feeds);
	}

}
