package boy.kuripot.model;

import boy.kuripot.view.HeadlineView;
import richard.john.romero.view.ProgressImageView;

public class ViewHolder
{
	private ProgressImageView iv;
	private HeadlineView headline;
	
	public ProgressImageView getIv()
	{
		return iv;
	}
	public void setIv(ProgressImageView iv)
	{
		this.iv = iv;
	}
	public HeadlineView getHeadline()
	{
		return headline;
	}
	public void setHeadline(HeadlineView headline)
	{
		this.headline = headline;
	}
}
