package boy.kuripot;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import boy.kuripot.model.Feed;
import boy.kuripot.share.EmailIntent;
import boy.kuripot.share.TwitterIntent;
import boy.kuripot.view.HeadlineView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class CcPromoInfoActivity extends SherlockFragmentActivity
		implements OnClickListener
{
	private HeadlineView headline;

	@Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promo_info);
        
        ActionBar actionbar = getSupportActionBar();
        actionbar.setCustomView(R.layout.promo_info_headline);
        actionbar.setDisplayOptions(
        		ActionBar.DISPLAY_SHOW_CUSTOM,
        		ActionBar.DISPLAY_SHOW_CUSTOM
        		| ActionBar.DISPLAY_SHOW_TITLE
        		| ActionBar.DISPLAY_SHOW_HOME
        		| ActionBar.DISPLAY_HOME_AS_UP);
        
        headline = (HeadlineView) findViewById(R.id.promo_info_headline_view);
        headline.setSelected(true);
        headline.setTextColor(Color.BLACK);
        headline.setOnClickListener(this);
        
        WebView wv = (WebView) findViewById(R.id.promo_info_wv);
        
        Feed feed = getIntent().getParcelableExtra(getString(R.string.feed_tag));
        if (feed != null)
        {
        	headline.setFeed(feed);
            String html = feed.getContent();
            
            int sdk_version = android.os.Build.VERSION.SDK_INT;
            if (sdk_version < android.os.Build.VERSION_CODES.HONEYCOMB)
            	html = Html.fromHtml(html).toString();
            	
//            wv.loadData(html, "text/html", "UTF-8");
            wv.loadData(html, "text/html; charset=UTF-8", null);
        }
    }

    @Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
    	setResult(0, new Intent().putExtra(getString(R.string.feed_tag), headline.getFeed()));
		super.onStop();
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.promo_info, menu);
        return true;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
        case android.R.id.home:
        case R.id.abs__home:
            NavUtils.navigateUpTo(this, new Intent(this, CcPromoListActivity.class));
            break;
        /*case R.id.promo_info_share_facebook:
        	break;*/
        case R.id.promo_info_share_twitter:
        	new TwitterIntent(this);
        	break;
        case R.id.promo_info_share_email:
        	new EmailIntent(this);
        	break;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		setResult(0, new Intent().putExtra(getString(R.string.feed_tag), headline.getFeed()));
		super.onBackPressed();
//		finish();
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.promo_info_headline_view:
			
			setResult(0, new Intent().putExtra(getString(R.string.feed_tag), headline.getFeed()));
//			NavUtils.navigateUpTo(this, new Intent(this, CcPromoListActivity.class));
			finish();
			break;
		}
	}
}
