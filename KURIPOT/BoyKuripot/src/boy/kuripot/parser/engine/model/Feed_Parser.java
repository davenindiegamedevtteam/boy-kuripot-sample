package boy.kuripot.parser.engine.model;

import java.util.List;

import boy.kuripot.model.Feed;

public interface Feed_Parser 
{
	public List<Feed> Parse();
}
