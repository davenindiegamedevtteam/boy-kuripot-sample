package boy.kuripot.parser.engine;

import android.os.Message;
import boy.kuripot.R;
import boy.kuripot.model.ParserEngine;
import boy.kuripot.parser.engine.controller.SAX_Feed_Parser;

public class AtomParserEngine extends ParserEngine
{

	public AtomParserEngine(Callback callback)
	{
		super(callback);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onMessageReceived(Message msg)
	{
		// TODO Auto-generated method stub
		switch (msg.what)
		{
		case R.id.request_feed:
			String url = (String) msg.obj;
			new SAX_Feed_Parser(callback, url).Parse();
			break;

		default:
			break;
		}
	}

}
