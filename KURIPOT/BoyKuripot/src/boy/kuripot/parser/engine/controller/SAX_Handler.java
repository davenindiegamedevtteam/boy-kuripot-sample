package boy.kuripot.parser.engine.controller;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;
import boy.kuripot.model.Feed;
import boy.kuripot.model.ParserEngine.Callback;

public class SAX_Handler extends DefaultHandler
{
	//TODO _________________________[ Data Fields ]_________________________
	private final Pattern pattern = Pattern.compile("(?<=src=\")[^\"]*(?=\")");
	private List<Feed> messages;
	private Feed current_msg;
	private StringBuilder builder;
	
	private Callback callback;
	
	public SAX_Handler(Callback callback)
	{
		this.callback = callback;
	}
	
	
	
	
	
	
	//TODO _________________________[ Get Method for Message ]_________________________
	public List<Feed> Get_Messages()
	{
		return this.messages;
	}
	
	
	
	
	
	
	
	
	
	
	//TODO _________________________[ Default Handler Methods ]_________________________
	public void startDocument() throws SAXException
	{
		super.startDocument();
		messages = new ArrayList<Feed>();
		builder = new StringBuilder();
	}
	
	
	
	public void endDocument()
	{
		System.out.println("PARSING COMPLETE. \n");
		callback.onParseFinished(messages);
	}
	
	
	
	public void startElement(String URI, String local_name, String Q_name, Attributes attributes) throws SAXException
	{
		super.startElement(URI, local_name, Q_name, attributes);
		
		if((local_name.equalsIgnoreCase(Base_Feed_Parser.ITEM)) || (local_name.equalsIgnoreCase(Base_Feed_Parser.ENTRY)))
		{
			this.current_msg = new Feed();
		}
	}
	
	
	
	public void endElement(String URI, String local_name, String Q_name) throws SAXException
	{
		super.endElement(URI, local_name, Q_name);
		
		if(this.current_msg != null)
		{
			if(local_name.equalsIgnoreCase(Base_Feed_Parser.TITLE))
			{
				current_msg.setTitle(builder.toString());
				
			} 
//			else if(local_name.equalsIgnoreCase(Base_Feed_Parser.LINK)) {
//				
//				current_msg.setLink(builder.toString());
//				
//			}
			else if(local_name.equalsIgnoreCase(Base_Feed_Parser.ID)) {
				String id = builder.substring(builder.indexOf("post-") + 5);
				current_msg.setBlogEntryId(id);
//				Matcher matcher = pattern.matcher(builder.toString());
//				String id = matcher.group();
//				System.out.println(id);
			}
			else if(local_name.equalsIgnoreCase(Base_Feed_Parser.DESCRIPTION)) {
				
				current_msg.setContent(Base_Feed_Parser.DESCRIPTION);
				
			} else if(local_name.equalsIgnoreCase(Base_Feed_Parser.SUBTITLE)) {
				
				current_msg.setContent(Base_Feed_Parser.SUBTITLE);
				
			} else if(local_name.equalsIgnoreCase(Base_Feed_Parser.CONTENT)) {

				Matcher matcher = pattern.matcher(builder.toString());
				String fString = null;
				while (matcher.find())
				{
					String imageUrl = matcher.group();
					current_msg.setImageUrl(imageUrl);
//					imageUrl = "file://sdcard/boykuripot/cache/" + String.valueOf(imageUrl.hashCode());
					imageUrl = "content://boy.kuripot" + getBaseURL() + String.valueOf("/" + imageUrl.hashCode());
//					Log.i("SAX_Handler", imageUrl);
					fString = matcher.replaceAll(imageUrl);
				}
				current_msg.setContent(fString);
				
			} else if(local_name.equalsIgnoreCase(Base_Feed_Parser.PUB_DATE) || local_name.equalsIgnoreCase(Base_Feed_Parser.PUBLISHED)) {
				
				current_msg.setDatePublished(builder.toString());
				
			}
			else if (local_name.equalsIgnoreCase(Base_Feed_Parser.UPDATED))
			{
				current_msg.setDateUpdated(builder.toString());
				
			}
			else if((local_name.equalsIgnoreCase(Base_Feed_Parser.ITEM)) || (local_name.equalsIgnoreCase(Base_Feed_Parser.ENTRY))) {
				
				messages.add(current_msg);
				
			}
			
			builder.setLength(0);
		}
	}
	
	private String getBaseURL()
	{
		File cacheDir = null;
		String sdState = android.os.Environment.getExternalStorageState();
		if (sdState.equals(android.os.Environment.MEDIA_MOUNTED))
		{
			File sdDir = android.os.Environment.getExternalStorageDirectory();		
			cacheDir = new File(sdDir, "boykuripot/cache");
		}
//		else
//			cacheDir = getActivity().getCacheDir();
		
		if(!cacheDir.exists())
			cacheDir.mkdirs();
		
		return cacheDir.getPath();
	}
	
	public void characters(char[] ch, int start, int length) throws SAXException
	{
		super.characters(ch, start, length);
		builder.append(ch, start, length);
	}
}
