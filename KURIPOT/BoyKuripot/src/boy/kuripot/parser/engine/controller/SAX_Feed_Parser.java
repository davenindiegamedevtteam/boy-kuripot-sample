package boy.kuripot.parser.engine.controller;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import boy.kuripot.model.Feed;
import boy.kuripot.model.ParserEngine.Callback;

import android.util.Log;

public class SAX_Feed_Parser extends Base_Feed_Parser
{
	private Callback callback;
	
	//TODO _________________________[ XML Parser Class' Constructor ]_________________________
	public SAX_Feed_Parser(Callback callback, String feed_url)
	{
		super(feed_url);
		this.callback = callback;
	}
	
	
	
	//TODO _________________________[ Parse Feed Method ]_________________________
	@Override
	public List<Feed> Parse() 
	{
		SAXParserFactory factory = SAXParserFactory.newInstance();
		
		try
		{
			SAXParser parser = factory.newSAXParser();
			SAX_Handler handler = new SAX_Handler(callback);
			InputStream is = this.Get_Input_Stream();
			
			if (is == null)
			{
				Log.i("Sax_Feed_Parser", "inputstream is null");
			}
			
			parser.parse(is, handler);
			return handler.Get_Messages();
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		return null;
	}
}
