package boy.kuripot.parser.engine.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import boy.kuripot.parser.engine.model.Feed_Parser;

public abstract class Base_Feed_Parser implements Feed_Parser
{
	//TODO _________________________[ Data Fields ]_________________________
	static final String ID = "id";
	
	// XML Tags (RSS)
	static final String PUB_DATE = "pubDate";
	static final String DESCRIPTION = "description";
	static final String LINK = "link";
	static final String TITLE = "title";
	static final String ITEM = "item";
	
	// XML Tags (Atom)
	static final String PUBLISHED = "published";
	static final String UPDATED = "updated";
	static final String SUBTITLE = "subtitle";
	static final String ENTRY = "entry";
	
	// XML Tags (? ? ?)
	static final String CONTENT = "content";
	
	// URL Link
	private URL feed_URL;
	
	
	
	
	
	
	
	
	
	
	//TODO _________________________[ Base Feed Parser's Abstract Constructor ]_________________________
	public Base_Feed_Parser(String feed_URL)
	{
		try
		{
			this.feed_URL = new URL(feed_URL);
			
		} catch(MalformedURLException e) {
			
			e.printStackTrace();
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	//TODO _________________________[ Method for Getting Input Stream ]_________________________
	protected InputStream Get_Input_Stream()
	{
		try
		{
			return feed_URL.openConnection().getInputStream();
			
		} catch(IOException e) {
			
			new RuntimeException(e);
			
		}
		return null;
	}
}
