package boy.kuripot;

import java.util.Observable;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import boy.kuripot.model.Feed;
import boy.kuripot.model.PromoFragment;
import boy.kuripot.model.ScreenMode;
import boy.kuripot.share.EmailIntent;
import boy.kuripot.share.TwitterIntent;
import boy.kuripot.view.HeadlineView;

public class PromoInfoFragment extends PromoFragment
		implements OnClickListener
{
	private final static int version = android.os.Build.VERSION.SDK_INT;
	private HeadlineView headline;
	private Feed feed;
	private WebView wv;
	
	@Override
	protected void initActionBar(ActionBar actionbar)
	{
		// TODO Auto-generated method stub
		actionbar.setCustomView(R.layout.promo_info_headline);
        actionbar.setDisplayOptions(
        		ActionBar.DISPLAY_SHOW_CUSTOM,
        		ActionBar.DISPLAY_SHOW_CUSTOM
        		| ActionBar.DISPLAY_SHOW_TITLE
        		| ActionBar.DISPLAY_SHOW_HOME
        		| ActionBar.DISPLAY_HOME_AS_UP);
	}

	@Override
	protected void initActionBarCustomView(View v)
	{
		// TODO Auto-generated method stub
		headline = (HeadlineView) v.findViewById(R.id.promo_info_headline_view);
        headline.setSelected(true);
        headline.setTextColor(Color.BLACK);
        headline.setOnClickListener(this);
        if (feed != null)
        	headline.setFeed(feed);
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		
		if (savedInstanceState != null)
        {
			feed = savedInstanceState.getParcelable(getString(R.string.feed_tag));
        }
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// TODO Auto-generated method stub
		outState.putParcelable(getString(R.string.feed_tag), feed);
		super.onSaveInstanceState(outState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.promo_info, container, false);
		
		Bundle b = getArguments();
		if (b != null && feed == null)
			feed = getArguments().getParcelable(getString(R.string.feed_tag));
		
		wv = (WebView) v.findViewById(R.id.promo_info_wv);
        refresh();
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// TODO Auto-generated method stub
		if (menu.findItem(R.id.promo_info_share) == null)
			inflater.inflate(R.menu.promo_info, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
//        case android.R.id.home:
//        case R.id.abs__home:
//            break;
        /*case R.id.promo_info_share_facebook:
        	break;*/
        case R.id.promo_info_share_twitter:
        	new TwitterIntent(getActivity());
        	break;
        case R.id.promo_info_share_email:
        	new EmailIntent(getActivity());
        	break;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void update(Observable observable, Object data)
	{
		// TODO Auto-generated method stub
		if (data == null)
			return;
		if (data instanceof Feed)
		{
			feed = (Feed) data;
			
//			if (!isDetached())
			if (!isDetached() && getScreenMode() == ScreenMode.NORMAL_PANE)
				initActionBarCustomView(getSherlockActivity()
					.getSupportActionBar().getCustomView());
	        
			refresh();
		}
		else if (data instanceof Integer && headline != null)
		{
			TextView tv = (TextView) headline.findViewById(R.id.headline_view_manage_tv);
			tv.setText(String.valueOf(data));
		}
		else 
			Log.i(getTag(), "data is not instance of Feed");
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.promo_info_headline_view:
			FragmentManager mgr = getActivity().getSupportFragmentManager();
			Fragment fragment = mgr.findFragmentByTag("list");
			if (fragment != null && fragment.isDetached())
			{
				FragmentTransaction txn = mgr.beginTransaction();
				txn.detach(this);
				txn.attach(fragment);
				txn.commit();
			}
			break;
		}
	}
	
	private void refresh()
	{
        if (feed != null && wv != null)
        {
            String html = feed.getContent();
            
            if (version < android.os.Build.VERSION_CODES.HONEYCOMB)
            	html = Html.fromHtml(html).toString();
            	
            wv.loadDataWithBaseURL("", html, "text/html", "charset=UTF-8", "");
        }
	}

}
