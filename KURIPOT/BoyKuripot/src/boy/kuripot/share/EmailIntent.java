package boy.kuripot.share;

import boy.kuripot.model.ShareIntent;
import android.content.Context;
import android.content.Intent;

public class EmailIntent extends ShareIntent
{

	public EmailIntent(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
		Intent emailIntent = new Intent();
    	emailIntent.setType("plain/text");
    	emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"frichromero@yahoo.com"});  
    	emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Share App"); 
    	emailIntent.putExtra(Intent.EXTRA_TEXT, "I would like to share an app called. Click the link to download. (Link)");
    	
    	//sample formatted text for email
//    	emailIntent.putExtra(
//    			Intent.EXTRA_TEXT,
//    			Html.fromHtml(new StringBuilder()
//    			    .append("<p><b>Some Content</b></p>")
//    			    .append("<small><p>More content</p></small>")
//    			    .toString()));
    	
    	emailIntent.setAction(Intent.ACTION_SEND);
    	emailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		context.startActivity(Intent.createChooser(emailIntent, "Email:"));
	}

}
