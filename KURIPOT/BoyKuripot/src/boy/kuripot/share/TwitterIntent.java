package boy.kuripot.share;

import java.util.List;

import boy.kuripot.model.ShareIntent;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.widget.Toast;

public class TwitterIntent extends ShareIntent
{

	public TwitterIntent(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
		try
		{
		    Intent intent = new Intent(Intent.ACTION_SEND);
		    intent.putExtra(Intent.EXTRA_TEXT, "test");
		    intent.setType("text/plain");
		    final PackageManager pm = context.getPackageManager();
		    final List<?> activityList = pm.queryIntentActivities(intent, 0);
		        int len =  activityList.size();
		    for (int i = 0; i < len; i++) {
		        final ResolveInfo app = (ResolveInfo) activityList.get(i);
		        if ("com.twitter.android.PostActivity".equals(app.activityInfo.name))
		        {
		            final ActivityInfo activity=app.activityInfo;
		            final ComponentName name=new ComponentName(activity.applicationInfo.packageName, activity.name);
		            intent=new Intent(Intent.ACTION_SEND);
		            intent.addCategory(Intent.CATEGORY_LAUNCHER);
		            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
		            intent.setComponent(name);
//		            intent.putExtra(Intent.EXTRA_TEXT, "test2");
		            context.startActivity(intent);
		            break;
		        }
		    }
		}
		catch (final ActivityNotFoundException e)
		{
		    Toast.makeText(context, "Official Twitter application cannot be found",
		    		Toast.LENGTH_LONG).show();
		}
		catch (Exception e)
		{
			Toast.makeText(context, e.toString(),
		    		Toast.LENGTH_LONG).show();
		}
	}
	
}
