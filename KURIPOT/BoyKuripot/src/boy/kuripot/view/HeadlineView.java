package boy.kuripot.view;

import java.util.List;

import boy.kuripot.R;
import boy.kuripot.model.Feed;
import boy.kuripot.model.PromoActivity;
import boy.kuripot.model.Receipt;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HeadlineView extends LinearLayout
		implements OnClickListener
{
	private final static String prefix = "Posted on: ";
	private TextView title, date, receiptsCount;
	private Feed feed;

	public HeadlineView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		// TODO Auto-generated constructor stub
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.HeadlineView, 0, 0);
		
		boolean upIndicatorEnabled = a.getBoolean(
				R.styleable.HeadlineView_up_indicator_enabled, false);
		int upIndicatorResource = a.getResourceId(
				R.styleable.HeadlineView_up_indicator, android.R.attr.homeAsUpIndicator);
		a.recycle();
		
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.headline_view, this, true);
		
		if (upIndicatorEnabled)
		{
			Log.i("HeadlineView", String.valueOf(upIndicatorEnabled));
			ImageView up = new ImageView(context);
			LayoutParams params = new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.gravity = Gravity.CENTER;
			up.setLayoutParams(params);
			up.setImageResource(upIndicatorResource);
			addView(up, 0);
		}
		
		title = (TextView) findViewById(R.id.headline_view_title_tv);
		date = (TextView) findViewById(R.id.headline_view_date_tv);
		receiptsCount = (TextView) findViewById(R.id.headline_view_manage_tv);
		receiptsCount.setOnClickListener(this);
	}
	
	public void setFeed(Feed feed)
	{
		this.feed = feed;
		title.setText(feed.getTitle());
		date.setText(prefix + feed.getFormattedDatePublished());
		receiptsCount.setText(String.valueOf(getAvailableCount(this.feed.getReceipts())));
	}
	
	public Feed getFeed()
	{
		return this.feed;
	}

	private int getAvailableCount(List<Receipt> receipts)
	{
		int count = 0;
		for (int i=0; i<receipts.size(); i++)
		{
			Receipt receipt = receipts.get(i);
			if (receipt.getDateCreated() > 0 &&
					receipt.getDateClaimed() == 0)
				count++;
		}
		return count;
	}
	
	public void setTextColor(int color)
	{
		this.title.setTextColor(color);
		this.date.setTextColor(color);
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		PromoActivity a = (PromoActivity) getContext();
		a.onContentChanged(this);
		
		Bundle bundle = new Bundle();
		bundle.putParcelable(a.getString(R.string.feed_tag), feed);
		
		DialogFragment dialog = new ChargeSlipDialog();
		dialog.setArguments(bundle);
		dialog.show(a.getSupportFragmentManager(), null);
	}

}
