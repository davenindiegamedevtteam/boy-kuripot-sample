package boy.kuripot.view;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import boy.kuripot.R;
import boy.kuripot.db.ReceiptDBHelper;
import boy.kuripot.model.BoyKuripotDialog;
import boy.kuripot.model.Feed;
import boy.kuripot.model.OnContentChangedCallback;
import boy.kuripot.model.PromoActivity;
import boy.kuripot.model.Receipt;

public class ChargeSlipDialog extends BoyKuripotDialog
		implements OnClickListener
{
	private final static String tag = ChargeSlipDialog.class.getSimpleName();
	private Feed feed;
	private OnContentChangedCallback callback;
	private ReceiptDBHelper helper;
	private TextView availableTv, claimedTv;

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try
		{
			PromoActivity a = (PromoActivity) activity;
			callback = a;
			Log.i(getTag(), "onAttach");
		}
		catch (ClassCastException e)
		{
			throw new ClassCastException(
					"Activity must be of type PromoActivity");
		}
		helper = new ReceiptDBHelper(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null)
        {
			feed = savedInstanceState.getParcelable(getString(R.string.feed_tag));
        }
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// TODO Auto-generated method stub
		outState.putParcelable(getString(R.string.feed_tag), feed);
		super.onSaveInstanceState(outState);
	}

	@Override
	public View onInflateContent(LayoutInflater inflater)
	{
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.charge_slip_dialog, null);
		
		Bundle b = getArguments();
		if (b != null)
		{
			feed = b.getParcelable(getString(R.string.feed_tag));
			if (feed == null)
				feed = new Feed();
		}
		
		availableTv = (TextView) v.findViewById(R.id.charge_slip_dialog_available_count_tv);
		claimedTv = (TextView) v.findViewById(R.id.charge_slip_dialog_claimed_count_tv);
		
		updateView();
		
		v.findViewById(R.id.charge_slip_dialog_add_iv).setOnClickListener(this);
		v.findViewById(R.id.charge_slip_dialog_claim_iv).setOnClickListener(this);
		v.findViewById(R.id.charge_slip_dialog_exit_iv).setOnClickListener(this);
		return v;
	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub
		callback.onContentChanged(feed.getReceipts());
		super.onDetach();
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		List<Receipt> receipts = feed.getReceipts();
		switch (v.getId())
		{
		case R.id.charge_slip_dialog_add_iv:
			Receipt receipt = new Receipt();
			receipt.setBlogEntryId(feed.getBlogEntryId());
			receipt.setDateCreated(System.currentTimeMillis());
			receipts.add(receipt);
			updateView();
			helper.createReceipt(receipt);
			break;
		case R.id.charge_slip_dialog_claim_iv:
			for (int i=0; i<receipts.size(); i++)
			{
				Receipt r = receipts.get(i);
				if (r.getDateCreated() > 0 && 
						r.getDateClaimed() == 0)
				{
					r.setDateClaimed(System.currentTimeMillis());
					updateView();
					helper.claimReceipt(r);
					return;
				}
			}
			break;
		case R.id.charge_slip_dialog_exit_iv:
			dismiss();
			break;
		}
	}
	
	private void updateView()
	{
		availableTv.setText(String.valueOf(getAvailableCount()));
		claimedTv.setText(String.valueOf(getClaimedCount()));
	}
	
	private int getAvailableCount()
	{
		List<Receipt> receipts = feed.getReceipts();
		int count = 0;
		for (int i=0; i<receipts.size(); i++)
		{
			Receipt receipt = receipts.get(i);
			if (receipt.getDateCreated() > 0 &&
					receipt.getDateClaimed() == 0)
				count++;
		}
		callback.onContentChanged(Integer.valueOf(count));
		return count;
	}
	
	private int getClaimedCount()
	{
		List<Receipt> receipts = feed.getReceipts();
		int count = 0;
		for (int i=0; i<receipts.size(); i++)
		{
			Receipt receipt = receipts.get(i);
			if (receipt.getDateCreated() > 0 &&
					receipt.getDateClaimed() > 0)
				count++;
		}
		return count;
	}

}
