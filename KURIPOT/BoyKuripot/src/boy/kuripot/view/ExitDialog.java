package boy.kuripot.view;

import boy.kuripot.R;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class ExitDialog extends DialogFragment
		implements OnClickListener
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME, 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.exit_dialog, null);
		Button btn = (Button) v.findViewById(R.id.btn_exit_yes);
		btn.setOnClickListener(this);
		btn = (Button) v.findViewById(R.id.btn_exit_no);
		btn.setOnClickListener(this);
		return v;
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.btn_exit_yes:
			getActivity().finish();
			break;
		case R.id.btn_exit_no:
			dismiss();
			break;
		}
	}

}
