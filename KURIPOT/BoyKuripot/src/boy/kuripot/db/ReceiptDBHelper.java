package boy.kuripot.db;

import java.util.ArrayList;
import java.util.List;

import boy.kuripot.model.Receipt;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ReceiptDBHelper extends DBHelper
{
//	private final static String tag = ReceiptDBHelper.class.getSimpleName();

	public ReceiptDBHelper(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public int createReceipt(Receipt receipt)
	{
		return createReceipt(receipt, null);
	}
	
	public int createReceipt(Receipt receipt, SQLiteDatabase db)
	{
		int num = 0;
		boolean isLocalDb = false;
		try
		{
			if (db == null)
			{
				db = getWritableDB();
				isLocalDb = true;
			}
			ContentValues values = new ContentValues();
			
			//values.put(FeedDBSchema.COL_ID, feed.getId());
			values.put(ReceiptDBSchema.COL_BLOG_ENTRY_ID, receipt.getBlogEntryId());
			values.put(ReceiptDBSchema.COL_DATE_CREATED, receipt.getDateCreated());
//			values.put(ReceiptDBSchema.COL_DATE_CLAIMED, receipt.getDateClaimed());
			
			db.insert(ReceiptDBSchema.TABLE_NAME, ReceiptDBSchema.COL_DATE_CLAIMED, values);
			
			num = getAvailableReceipts(db);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if (db != null && db.isOpen() && isLocalDb)
			{
				db.close();
			}
		}
		return num;
	}
	
	public int getAvailableReceipts(SQLiteDatabase db)
	{
		int num = 0;
		boolean isLocalDb = false;
		String[] columns = {ReceiptDBSchema.COL_DATE_CLAIMED};
		String selection = new StringBuilder(ReceiptDBSchema.COL_DATE_CLAIMED)
											.append("=")
											.append("null").toString();
		try
		{
			if (db == null)
			{
				db = getReadableDB();
				isLocalDb = true;
			}
			Cursor c = db.query(ReceiptDBSchema.TABLE_NAME,
					columns, selection, null, null, null, null);
			num = c.getCount();
			c.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if (db != null && db.isOpen() && isLocalDb)
			{
				db.close();
			}
		}
		return num;
	}
	
	public List<Receipt> getReceiptsById(String blogEntryId, SQLiteDatabase db)
	{
		List<Receipt> receipts = new ArrayList<Receipt>();
		boolean isLocalDb = false;
		String selection = new StringBuilder(ReceiptDBSchema.COL_BLOG_ENTRY_ID)
											.append("='")
											.append(blogEntryId)
											.append("'").toString();
		try
		{
			if (db == null)
			{
				db = getReadableDB();
				isLocalDb = true;
			}
			Cursor c = db.query(ReceiptDBSchema.TABLE_NAME,
					null, selection, null, null, null, null);
			
			int iCOL_BLOG_ENTRY_ID = c.getColumnIndex(ReceiptDBSchema.COL_BLOG_ENTRY_ID);
			int iCOL_DATE_CREATED = c.getColumnIndex(ReceiptDBSchema.COL_DATE_CREATED);
			int iCOL_DATE_CLAIMED = c.getColumnIndex(ReceiptDBSchema.COL_DATE_CLAIMED);
			
			if (c.moveToFirst())
			{
				while (!c.isAfterLast())
				{
					Receipt receipt = new Receipt();
					receipt.setBlogEntryId(c.getString(iCOL_BLOG_ENTRY_ID));
					receipt.setDateCreated(c.getLong(iCOL_DATE_CREATED));
					receipt.setDateClaimed(c.getLong(iCOL_DATE_CLAIMED));
//					Log.i(tag, receipt.toString());
					receipts.add(receipt);
					c.moveToNext();
				}
			}
			c.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if (db != null && db.isOpen() && isLocalDb)
			{
				db.close();
			}
		}
		return receipts;
	}
	
	public long claimReceipt(Receipt receipt)
	{
		return claimReceipt(receipt, null);
	}
	
	public long claimReceipt(Receipt receipt, SQLiteDatabase db)
	{
		long row = -1;
		boolean isLocalDb = false;
		String whereClause = new StringBuilder()
				.append(ReceiptDBSchema.COL_DATE_CREATED)
				.append("=")
				.append(receipt.getDateCreated())
				.append("").toString();
		try
		{
			if (db == null)
			{
				db = getWritableDB();
				isLocalDb = true;
			}
			ContentValues values = new ContentValues();
			
			//values.put(FeedDBSchema.COL_ID, feed.getId());
			values.put(ReceiptDBSchema.COL_DATE_CLAIMED, receipt.getDateClaimed());
//			Log.i(tag, whereClause);
			row = db.update(ReceiptDBSchema.TABLE_NAME, values, whereClause, null);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			row = -1;
		}
		finally
		{
			if (db != null && db.isOpen() && isLocalDb)
			{
				db.close();
			}
		}
		return row;
	}

}
