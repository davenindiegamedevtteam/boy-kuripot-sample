package boy.kuripot.db;

import java.io.File;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class DBHelper
{
	private final static String DB_NAME = "BoyKuripot.db";
	private Context context;
	private DBOpenHelper _helper;

	public DBHelper(Context context)
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		_helper = new DBOpenHelper(context, getDBPathName(DB_NAME));
	}
	
	public Context getContext()
	{
		return context;
	}

	protected synchronized SQLiteDatabase getReadableDB()
	{
		return _helper.getReadableDatabase();
	}
	
	protected synchronized SQLiteDatabase getWritableDB()
	{
		return _helper.getWritableDatabase();
	}
	
	private String getDBPathName(String name)
	{
		File dbDir;
		String sdState = Environment.getExternalStorageState();
		if (sdState.equals(Environment.MEDIA_MOUNTED)) {
			File sdDir = Environment.getExternalStorageDirectory();	
			dbDir = new File(sdDir.getPath(), "boykuripot/db");
		}
		else
			return null;
		
		if (!dbDir.exists())
			dbDir.mkdirs();
		
		File dbFile = new File(dbDir.getPath(), name);
		return dbFile.getPath();
	}

}
