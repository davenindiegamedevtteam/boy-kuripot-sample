package boy.kuripot.db;

public class FeedDBSchema
{
	//"ID, BLOG_ENTRY_ID, TITLE, CONTENT, URL, DATE_PUBLISHED, DATE_UPDATED"
	public final static String TABLE_NAME = "FEEDS";
	public final static String COL_ID = "id";
	public final static String COL_BLOG_ENTRY_ID = "blog_entry_id";
	public final static String COL_TITLE = "title";
	public final static String COL_CONTENT = "content";
	public final static String COL_URL = "url";
	public final static String COL_IMAGE_URL = "image_url";
	public final static String COL_DATE_PUBLISHED = "date_published";
	public final static String COL_DATE_UPDATED = "date_updated";
}
