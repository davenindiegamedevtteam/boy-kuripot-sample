package boy.kuripot.db;

public class ReceiptDBSchema
{
	//"ENTRY_ID, DATE_CREATED, DATE_CLAIMED"
	public final static String TABLE_NAME = "RECEIPTS";
	public final static String COL_BLOG_ENTRY_ID = "blog_entry_id";
	public final static String COL_DATE_CREATED = "date_created";
	public final static String COL_DATE_CLAIMED = "date_claimed";
}
