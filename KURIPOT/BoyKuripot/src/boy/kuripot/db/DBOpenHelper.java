package boy.kuripot.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class DBOpenHelper extends SQLiteOpenHelper
{
	private final static int dbVersion = 1;

	public DBOpenHelper(Context context, String name)
	{
		super(context, name, null, dbVersion);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		// TODO Auto-generated method stub
		//"ID, BLOG_ENTRY_ID, TITLE, CONTENT, URL, DATE_PUBLISHED, DATE_UPDATED"
		db.execSQL("CREATE TABLE IF NOT EXISTS "   
	        + FeedDBSchema.TABLE_NAME + " ("
	        + FeedDBSchema.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
	        + FeedDBSchema.COL_BLOG_ENTRY_ID + " VARCHAR,"
	        + FeedDBSchema.COL_TITLE + " VARCHAR,"
	        + FeedDBSchema.COL_CONTENT + " VARCHAR,"
	        + FeedDBSchema.COL_URL + " VARCHAR,"
	        + FeedDBSchema.COL_IMAGE_URL + " VARCHAR,"
	        + FeedDBSchema.COL_DATE_PUBLISHED + " VARCHAR,"
			+ FeedDBSchema.COL_DATE_UPDATED + " VARCHAR)");
		
		//"ENTRY_ID, DATE_CREATED, DATE_CLAIMED"
		db.execSQL("CREATE TABLE IF NOT EXISTS "   
	        + ReceiptDBSchema.TABLE_NAME + " ("
	        + ReceiptDBSchema.COL_BLOG_ENTRY_ID + " VARCHAR," 
	        + ReceiptDBSchema.COL_DATE_CREATED + " LONG,"
			+ ReceiptDBSchema.COL_DATE_CLAIMED + " LONG NULL)");
		
		/*
		db.execSQL("CREATE TABLE IF NOT EXISTS "   
	        + MessagesDBSchema.TABLE_NAME + " ("
	        + MessagesDBSchema.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
	        + MessagesDBSchema.COL_MESSAGE + " VARCHAR," 
	        + MessagesDBSchema.COL_JEPIN + " VARCHAR,"  
	        + MessagesDBSchema.COL_TIMESTAMP + " VARCHAR,"
	        + MessagesDBSchema.COL_TIMEZONE + " VARCHAR,"
	        + MessagesDBSchema.COL_READSTATE + " VARCHAR,"
			+ MessagesDBSchema.COL_BOUNDSTATE + " VARCHAR)");
		*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public synchronized SQLiteDatabase getReadableDatabase()
	{
		// TODO Auto-generated method stub
		//super.getReadableDatabase();
		return super.getReadableDatabase();
	}

	@Override
	public synchronized SQLiteDatabase getWritableDatabase()
	{
		// TODO Auto-generated method stub
		return super.getWritableDatabase();
	}
}
