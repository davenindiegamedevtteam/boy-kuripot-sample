package boy.kuripot.db;

import java.util.ArrayList;
import java.util.List;

import boy.kuripot.model.Feed;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PromoDBHelper extends DBHelper
{

	public PromoDBHelper(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public long addFeed(Feed feed)
	{
		return addFeed(feed, null);
	}
	
	public long addFeed(Feed feed, SQLiteDatabase db)
	{
		long row = -1;
		boolean isLocalDb = false;
		try
		{
			if (db == null)
			{
				db = getWritableDB();
				isLocalDb = true;
			}
			ContentValues values = new ContentValues();
			
			//values.put(FeedDBSchema.COL_ID, feed.getId());
			values.put(FeedDBSchema.COL_BLOG_ENTRY_ID, feed.getBlogEntryId());
			values.put(FeedDBSchema.COL_TITLE, feed.getTitle());
			values.put(FeedDBSchema.COL_CONTENT, feed.getContent());
			values.put(FeedDBSchema.COL_URL, feed.getUrl());
			values.put(FeedDBSchema.COL_IMAGE_URL, feed.getImageUrl());
			values.put(FeedDBSchema.COL_DATE_PUBLISHED, feed.getDatePublished());
			values.put(FeedDBSchema.COL_DATE_UPDATED, feed.getDateUpdated());
			
			row = db.insert(FeedDBSchema.TABLE_NAME, null, values);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if (db != null && db.isOpen() && isLocalDb)
			{
				db.close();
			}
		}
		return row;
	}
	
	public long addFeeds(List<Feed> feeds)
	{
		long row = -1;
		SQLiteDatabase db = null;
		try
		{
			db = getWritableDB();
			deleteFeeds(db);
			for (int i=0; i<feeds.size(); i++)
			{
				row = addFeed(feeds.get(i), db);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			row = -1;
		}
		finally
		{
			if (db != null && db.isOpen())
			{
				db.close();
			}
		}
		return row;
	}
	
	public long updateFeed(Feed feed)
	{
		long row = -1;
		SQLiteDatabase db = null;
		try
		{
			db = getWritableDB();
			String whereClause = FeedDBSchema.COL_BLOG_ENTRY_ID + "=?";
			String[] whereArgs = new String[] {feed.getBlogEntryId()};
			ContentValues values = new ContentValues();
			values.put(FeedDBSchema.COL_TITLE, feed.getTitle());
			values.put(FeedDBSchema.COL_CONTENT, feed.getContent());
			values.put(FeedDBSchema.COL_URL, feed.getUrl());
			values.put(FeedDBSchema.COL_IMAGE_URL, feed.getImageUrl());
			values.put(FeedDBSchema.COL_DATE_PUBLISHED, feed.getDatePublished());
			values.put(FeedDBSchema.COL_DATE_UPDATED, feed.getDateUpdated());
			row = db.update(FeedDBSchema.TABLE_NAME, values, whereClause, whereArgs);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			row = -1;
		}
		finally
		{
			if (db != null && db.isOpen())
			{
				db.close();
			}
		}
		
		return row;
	}
	
	public ArrayList<Feed> getFeeds()
	{
		ArrayList<Feed> feeds = new ArrayList<Feed>();
		SQLiteDatabase db = null;
		
		String table = FeedDBSchema.TABLE_NAME;
		String groupBy = FeedDBSchema.COL_DATE_UPDATED + " DESC";
		
		Cursor c = null;
		try
		{
			db = getReadableDB();
			c = db.query(table, null, null, null, null, null, groupBy);
			
//			int iCOL_ID = c.getColumnIndex(FeedDBSchema.COL_ID);
			int iCOL_BLOG_ENTRY_ID = c.getColumnIndex(FeedDBSchema.COL_BLOG_ENTRY_ID);
			int iCOL_TITLE = c.getColumnIndex(FeedDBSchema.COL_TITLE);
			int iCOL_CONTENT = c.getColumnIndex(FeedDBSchema.COL_CONTENT);
			int iCOL_URL = c.getColumnIndex(FeedDBSchema.COL_URL);
			int iCOL_IMAGE_URL = c.getColumnIndex(FeedDBSchema.COL_IMAGE_URL);
			int iCOL_DATE_PUBLISHED = c.getColumnIndex(FeedDBSchema.COL_DATE_PUBLISHED);
			int iCOL_DATE_UPDATED = c.getColumnIndex(FeedDBSchema.COL_DATE_UPDATED);
			
			if (c.moveToFirst())
			{
				ReceiptDBHelper helper = new ReceiptDBHelper(getContext());
				while (!c.isAfterLast())
				{
					Feed feed = new Feed();
					feed.setBlogEntryId(c.getString(iCOL_BLOG_ENTRY_ID));
					feed.setTitle(c.getString(iCOL_TITLE));
					feed.setContent(c.getString(iCOL_CONTENT));
					feed.setUrl(c.getString(iCOL_URL));
					feed.setImageUrl(c.getString(iCOL_IMAGE_URL));
					feed.setDatePublished(c.getString(iCOL_DATE_PUBLISHED));
					feed.setDateUpdated(c.getString(iCOL_DATE_UPDATED));
					feed.setReceipts(helper.getReceiptsById(feed.getBlogEntryId(), db));
					feeds.add(feed);
					c.moveToNext();
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if (c != null)
				c.close();
			if (db != null)
				db.close();
		}
		return feeds;
	}
	
	private long deleteFeeds(SQLiteDatabase db)
	{
		long row = -1;
		boolean isLocalDb = false;
		try
		{
			if (db == null)
			{
				db = getWritableDB();
				isLocalDb = true;
			}
			db = getWritableDB();
			row = db.delete(FeedDBSchema.TABLE_NAME, null, null);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			row = -1;
		}
		finally
		{
			if (db != null && db.isOpen() && isLocalDb)
			{
				db.close();
			}
		}
		return row;
	}

}
