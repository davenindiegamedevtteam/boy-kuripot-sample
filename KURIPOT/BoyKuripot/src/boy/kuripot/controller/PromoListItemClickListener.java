package boy.kuripot.controller;

import boy.kuripot.CcPromoInfoActivity;
import boy.kuripot.R;
import boy.kuripot.model.Feed;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class PromoListItemClickListener implements OnItemClickListener
{
	private Activity activity;

	public PromoListItemClickListener(Activity activity)
	{
		// TODO Auto-generated constructor stub
		this.activity = activity;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3)
	{
		// TODO Auto-generated method stub
		Feed feed = (Feed) view.getTag(R.string.feed_tag);
		Intent intent = new Intent(activity, CcPromoInfoActivity.class);
		intent.putExtra(activity.getString(R.string.feed_tag), feed);
		activity.startActivityForResult(intent, 0);
	}

}
