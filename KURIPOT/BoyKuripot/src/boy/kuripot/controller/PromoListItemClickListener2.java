package boy.kuripot.controller;

import boy.kuripot.R;
import boy.kuripot.model.Feed;
import boy.kuripot.model.FragmentInfo;
import boy.kuripot.model.PromoActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class PromoListItemClickListener2 implements OnItemClickListener
{	
	private Fragment curFragment;
	private int container;
	private Class<?> clz;
	private String tag;
	private PromoActivity activity;
	private FragmentManager mgr;
	private FragmentTransaction txn;
	
	public PromoListItemClickListener2(Fragment curFragment, int container, FragmentInfo info)
	{
		// TODO Auto-generated constructor stub
		this.curFragment = curFragment;
		this.container = container;
		this.clz = info.getClz();
		this.tag = info.getTag();
		this.activity = (PromoActivity) curFragment.getActivity();
		this.mgr = this.activity.getSupportFragmentManager();
		this.txn = this.mgr.beginTransaction();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3)
	{
		// TODO Auto-generated method stub
		Feed feed = (Feed) view.getTag(R.string.feed_tag);
		
		if (activity.findViewById(R.id.fragment_container) != null)
		{
			txn.detach(curFragment);
			
			Fragment newFragment = mgr.findFragmentByTag(tag);
//			if (newFragment == null)
//				newFragment = mgr.findFragmentById(R.id.main_promo_info);
			if (newFragment == null)
			{
				Bundle bundle = new Bundle();
				bundle.putParcelable(activity.getString(R.string.feed_tag), feed);
				
				newFragment = Fragment.instantiate(activity, clz.getName());
				newFragment.setArguments(bundle);
				txn.add(container, newFragment, tag);
				txn.addToBackStack(null);
				txn.commit();
				Log.i(tag, "fragment added");
			}
			else if (newFragment != null)
			{
				if (newFragment.isDetached())
				{
					txn.attach(newFragment);
					txn.addToBackStack(null);
					txn.commit();
				}
//				activity.onContentChanged(view);
				activity.onContentChanged(feed);
				Log.i(tag, "fragment attached");
			}
		}
		else
		{
			activity.onContentChanged(view);
			activity.onContentChanged(feed);
		}
	}

}
