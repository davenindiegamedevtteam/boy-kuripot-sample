package boy.kuripot.controller;

import java.util.List;

import richard.john.romero.ImageLoader;
import richard.john.romero.view.ProgressImageView;

import boy.kuripot.R;
import boy.kuripot.model.Feed;
import boy.kuripot.model.ViewHolder;
import boy.kuripot.view.HeadlineView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class CcPromoListArrayAdapter extends ArrayAdapter<Feed>
{
	private final LayoutInflater inflater = LayoutInflater.from(getContext()); 
	private ImageLoader loader = new ImageLoader(getContext(), "boykuripot/cache");

	public CcPromoListArrayAdapter(Context context, List<Feed> objects)
	{
		super(context, R.layout.promo_list_item, objects);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// TODO Auto-generated method stub
		View v = convertView;
		ViewHolder holder;
		if (v == null)
		{
			v = inflater.inflate(R.layout.promo_list_item, null);
			holder = new ViewHolder();
			holder.setIv((ProgressImageView) v.findViewById(R.id.promo_list_item_iv));
			holder.setHeadline((HeadlineView) v.findViewById(R.id.promo_list_item_headline_view));
			v.setTag(holder);
		}
		else
			holder = (ViewHolder) v.getTag();
		
		final Feed item = getItem(position);
		if (item != null)
		{
			HeadlineView headline = holder.getHeadline();
			headline.setFeed(item);
			v.setTag(R.string.feed_tag, item);
			loader.displayImage(item.getImageUrl(), holder.getIv());
		}
		return v;
	}

}
