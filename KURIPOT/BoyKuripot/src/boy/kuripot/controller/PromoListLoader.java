package boy.kuripot.controller;

import java.util.List;

import android.content.Context;
import boy.kuripot.db.PromoDBHelper;
import boy.kuripot.model.AsyncTaskListLoader;
import boy.kuripot.model.Feed;

public class PromoListLoader extends AsyncTaskListLoader<Feed>
{

	public PromoListLoader(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Feed> loadInBackground()
	{
		// TODO Auto-generated method stub
		PromoDBHelper helper = new PromoDBHelper(getContext());
		return helper.getFeeds();
	}

	@Override
	protected List<Feed> getData()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
