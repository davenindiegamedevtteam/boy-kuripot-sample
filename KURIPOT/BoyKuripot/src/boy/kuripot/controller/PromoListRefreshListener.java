package boy.kuripot.controller;

import android.widget.ListView;
import boy.kuripot.PromoListFragment;
import boy.kuripot.R;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

public class PromoListRefreshListener implements OnRefreshListener<ListView>
{
	private PromoListFragment fragment;
	private String url;

	public PromoListRefreshListener(PromoListFragment fragment)
	{
		// TODO Auto-generated constructor stub
		this.fragment = fragment;
		this.url = fragment.getString(R.string.url);
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView)
	{
		// TODO Auto-generated method stub
		fragment.sendMessage(R.id.request_feed, url);
	}

}
