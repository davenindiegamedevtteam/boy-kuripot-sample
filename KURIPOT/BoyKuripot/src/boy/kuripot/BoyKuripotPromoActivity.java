package boy.kuripot;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import boy.kuripot.controller.AboutUsOnClickListener;
import boy.kuripot.model.PromoActivity;
import boy.kuripot.model.ScreenMode;
import boy.kuripot.view.AboutUsDialog;
import boy.kuripot.view.ExitDialog;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class BoyKuripotPromoActivity extends PromoActivity
{

	@Override
	protected void onCreate(Bundle bundle)
	{
		// TODO Auto-generated method stub
		super.onCreate(bundle);
		setContentView(R.layout.main_promo);
		
		if (findViewById(R.id.fragment_container) != null)
		{
//			if (bundle != null)
//				return;
			setScreenMode(ScreenMode.NORMAL_PANE);
			FragmentManager mgr = getSupportFragmentManager();
			
			Fragment fragment = mgr.findFragmentByTag("list");
			if (fragment == null)
			{
				FragmentTransaction txn = mgr.beginTransaction();
				fragment = new PromoListFragment();
				txn.add(R.id.fragment_container, fragment, "list");
				txn.commit();
			}
		}
		else
		{
			ActionBar actionbar = getSupportActionBar();
			actionbar.setCustomView(R.layout.dual_pane_titlebar);
	        actionbar.setDisplayOptions(
	        		ActionBar.DISPLAY_SHOW_CUSTOM,
	        		ActionBar.DISPLAY_SHOW_CUSTOM
	        		| ActionBar.DISPLAY_SHOW_HOME
	        		| ActionBar.DISPLAY_SHOW_TITLE);
	        
	        View aboutUsIv = findViewById(R.id.dual_pane_titlebar_about_us_iv);
	        if (aboutUsIv == null)
	        	Log.i(getClass().getSimpleName(), "null");
	        else
	        aboutUsIv.setOnClickListener(
	        		new AboutUsOnClickListener(
	        				getSupportFragmentManager()));
		}
	}

	@SuppressLint("Recycle")
	protected void setFragment(Class<Fragment> clz, String tag)
	{
		FragmentManager mgr = getSupportFragmentManager();
		FragmentTransaction txn = mgr.beginTransaction();
		Fragment preInitFragment = mgr.findFragmentByTag(tag);
		if (preInitFragment == null)
		{
			Fragment fragment = Fragment.instantiate(this, clz.getName());
			txn.add(android.R.id.content, fragment, tag);
		}
		else if (preInitFragment != null)
		{
			txn.attach(preInitFragment);
			txn.commit();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
	// TODO Auto-generated method stub
		MenuInflater inflater = new MenuInflater(this);  //import com.actionbarsherlock.view.MenuInflater;
		if (menu.findItem(R.id.main_promo_about_us) == null)
			inflater.inflate(R.menu.main_promo, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
        /*case R.id.promo_info_share_facebook:
        break;*/
        case R.id.main_promo_about_us:
        	new AboutUsDialog().show(getSupportFragmentManager(), null);
        break;
        case R.id.main_promo_exit:
        	new ExitDialog().show(getSupportFragmentManager(), "exit");
        break;
        }
        return super.onOptionsItemSelected(item);
    }
	
	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		FragmentManager mgr = getSupportFragmentManager();
		if (mgr.findFragmentByTag("list") == null || !mgr.findFragmentByTag("list").isDetached())
		{
			new ExitDialog().show(getSupportFragmentManager(), null);
			
		} else {
			
			super.onBackPressed();
			
		}
	}

}
